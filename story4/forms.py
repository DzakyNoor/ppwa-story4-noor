from django import forms
from story4.models import Activity

class ActivityForm(forms.Form):

    attrs_text = {
        'class': 'form-control'
    }

    attrs_date = {
        'class': 'form-control',
        'type': 'date'
    }

    attrs_time = {
        'class': 'form-control',
        'type': 'time'
    }

    name = forms.CharField(label='Activity Name', required=True,
                               widget=forms.TextInput(attrs=attrs_text))
    
    date = forms.DateField(label = 'Date', required=True,
                    widget = forms.DateInput(attrs = attrs_date))

    time = forms.TimeField(label = 'Time', required=True,
                    widget = forms.TimeInput(attrs = attrs_time))

    location = forms.CharField(label='Location', required=False,
                            widget=forms.TextInput(attrs=attrs_text))

    category = forms.CharField(label='Category', required=False,
                            widget=forms.TextInput(attrs=attrs_text))