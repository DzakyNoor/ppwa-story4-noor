from django.shortcuts import render
from django.http import HttpResponseRedirect
from story4.models import Activity
from story4.forms import ActivityForm

response = {}
# Create your views here.
def home(request):
    return render(request, 'story4/home.html')

def register(request):
    return render(request, 'story4/register.html')

def project(request):
	return render(request, 'story4/project.html')

def main(request):
	return render(request, 'story4/main.html')

def schedule(request):
    try :
        return render(request, 'story4/schedule.html',{'activities_list' : Activity.objects.all()})
    except Exception as e :    
        return render(request, 'story4/schedule.html',{})

def schedule_form(request):
    try:
        return render(request, 'story4/schedule_form.html', {'Form' : ActivityForm})
    except Exception  as e:
        return render(request,'story4/schedule_form.html')


def schedule_submit(request):
    form = ActivityForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        act = Activity(name=response['name'],date=response['date'],time=response['time'],
        location=response['location'], category=response['category'])
        
        act.save()
        return HttpResponseRedirect('/story4/schedule/')
    else:
        return HttpResponseRedirect('/story4/')


def schedule_clear(request):
    try :
        Activity.objects.all().delete()
        return HttpResponseRedirect('/story4/schedule/')
    except Exception as e:
        return HttpResponseRedirect('/story4/schedule/')
