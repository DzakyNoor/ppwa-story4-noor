from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='Noor_Home'),
    path('register/', views.register, name='Noor_Register'),
    path('project/', views.project, name='Noor_Project'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule_form/', views.schedule_form, name='schedule_form'),
    path('schedule_submit/', views.schedule_submit, name='schedule_submit'),
    path('schedule_clear/', views.schedule_clear, name='schedule_clear'),
]
